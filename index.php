<?php 
	$GLOBALS['lawList']=[
    	    [
    			"name"=>"Constitución Española",
    			"description"=>"https://www.boe.es/eli/es/c/1978/12/27/(1)"
    	    ],
    	    [
    			"name"=>"Ley de Sociedades de Capital",
    			"description"=>"https://www.boe.es/eli/es/rdlg/2010/07/02/1/con"
			]
	];
	/*
	$GLOBALS['contractList']=[
    	    [
    			"name"=>"contrato de arrendamiento de vehículo",
    			"description"=>"El contrato de arrendamiento de vehículo, tiene como finalidad la cesión temporal del uso de un bien mueble entre particulares, a cambio del pago de una renta fija y para los fines exclusivos que se pacten. Para efectos del uso, el arrendatario hace la contratación de un seguro en caso de siniestros y se hace responsables en daños ocasionados a terceros.",
    			"url"=>"https://www.milcontratos.com/documento-legal/contrato-arrendamiento-vehiculo"
    	    ],
    	    [
    			"name"=>"contrato de renting de vehículo",
    			"description"=>"El contrato de renting de vehículo es el contrato mediante el cual se cede el uso de un vehículo por una duración determinada a cambio de una renta. Además de arrendar y ceder el uso del bien, la empresa arrendadora se compromete a prestar todos los servicios de mantenimiento y gestión del uso que deriven del bien.",
    			"url"=>"https://www.milcontratos.com/documento-legal/contrato-renting-vehiculo"
			],
    	    [
    			"name"=>"contrato de compraventa de vehículo usado entre particulares",
    			"description"=>"El contrato de compraventa de vehículo de segunda mano entre particulares, tiene como finalidad la transmisión de la propiedad de un automóvil usado. En este escrito el precio del vehículo estará sujeto al estado actual en que se pudiera encontrar, de tal suerte que las pruebas técnicas que el comprador o el tercero en su caso, realicen al bien mueble. Del mismo modo, se plantea la situación de que el método de pago cuente con diversas variantes para abonarlo, mientras que la totalidad del mismo será aportada en el momento de la celebración del contrato..",
    			"url"=>"https://www.milcontratos.com/documento-legal/contrato-compraventa-vehiculo-usado-particulares"
			]
	];
	*/
	$GLOBALS['directoryTree'] = [
		[
			"title"=>"",
			"label"=>"derecho mercantil"
		],
		[
			"title"=>"",
			"label"=>"derecho civil"
		],
		[
			"title"=>"",
			"label"=>"derecho mercantil"
		],
		[
			"title"=>"",
			"label"=>"derecho civil"
		],
		[
			"title"=>"",
			"label"=>"derecho mercantil, derecho civil"
		],
		[
			"title"=>"",
			"label"=>"derecho mercantil, otro derecho"
		]
	];
	//verificar action
	function verifyAction($update) {
	    //el action getInfoLaw que hemos indicado en el intent (action and parameters)
	    switch ($update["queryResult"]["action"]){
	        case 'getInfoLaw':
    	        //parametros creados en el intent.
    	    	getInfoLaw($update["queryResult"]["parameters"]);
    	    	break;
    	    case 'getInfoFaq':
    	    	getInfoFaq($update["queryResult"]["parameters"]);
    	    	break; 
    	    //En caso de action desconocido
    	    default:
    	        sendMessage(array(
    	            "fulfillmentText"=> "action <".$update['queryResult']['action']."> desconocido"
    	        ));
    	    break;
	    }
	}


	function getInfoFaq($params){
	    
		$vocabularioFAQ = $params['vocabularioFAQ'];
		switch ($vocabularioFAQ) {
			case 'documento':
				if (isset($params['claveRama'])) {
					$clave = $params['claveRama'];
					$num = 0;
					foreach ($GLOBALS['directoryTree'] as $key => $value) {
						$num += (strstr($value['label'],$clave))?1:0;
					}
					$result = ($num<=0)?"No hemos encontrado documento relacionado con $clave":($num==1)?"Tenemos 1 documento relacionado con $clave":"Tenemos $num documentos relacionado con $clave";
					sendMessage(array(
			            "fulfillmentText" => $result,
			        ));
				}else{
					sendMessage(array(
			            "fulfillmentText" => count($GLOBALS['directoryTree'])." y creciendo cada día",
			        ));
				}
				break;
			case 'horario':
				sendMessage(array(
		            "fulfillmentText" => "De lunes a jueves de 9:00 - 18:00 y los viernes desde 9:00 - 14:00.",
		        ));
				break;
			case 'telefono':
				sendMessage(array(
		            "fulfillmentText" => "Puedes llamarnos al teléfono gratuito 900 908 106. Será un placer ayudarte.",
		        ));
				break;
			default:
				# code...
				break;
		}
	}

	//obtenemos los informacions desde un json ($GLOBALS['lawList'])
	function getInfoLaw($params){
		//obtenemos el nombre de ley
	    $lawName = $params["ley"][0];
	    foreach ($GLOBALS['lawList'] as $key => $value) {
	    	if ($value['name'] === $lawName) {
	    		sendMessage(array(
		            "fulfillmentText" => $value['description'],
		        ));
	    	}
	    }
	    
	}
	//retornar resultado
	function sendMessage($parameters) {
	    echo json_encode($parameters);
	}
	
	$update_response = file_get_contents("php://input");
	$update = json_decode($update_response, true);
	if(isset($update["queryResult"]["action"])) {
	    verifyAction($update);
	}else{
		//mensaje de error
        sendMessage(array(
            "fulfillmentText"=> "Error, falta action"
        ));
	}
?>