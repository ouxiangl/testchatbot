# Chatbot

Comando para instalar **npm install -g jovo-cli**
npm WARN optional Skipping failed optional dependency /jovo-cli/chokidar/fsevents:
npm WARN notsup Not compatible with your operating system or architecture: fsevents@1.2.9

- Para crear un intencion simple:
	fichero .json (en-US.json)
		{
			"name": "MyNameIsIntent", //Nombre de intencion
			"phrases": [	//frases para activar intencion
				"{name}",	// {name} es el variable que se define cliente por entrada (inputs)
				"my name is {name}",
				"i am {name}",
				"you can call me {name}"
			],
			"inputs": [
				{
					"name": "",	// variable name
					"type": {	//tipo de variable
						"alexa": "AMAZON.US_FIRST_NAME",
						"dialogflow": "@sys.given-name"
					}
				}
			]
		}

	fichero app.js
		MyNameIsIntent() {
			this.tell('Hey ${this.$inputs.name.value}!'); //${this.$inputs.name.value} es el valor de variable name que había introducido cliente
		},

	Input: my name is Pepe
	Ouput: Hey Pepe

- Para crear una intencion de forma inLine
	fichero .json (en-US.json)
		{
			"name": "YesIntent",
			"phrases": [
				"yes"
			]
		},
		{
			"name": "NoIntent",
			"phrases": [
				"no"
			]
		}

	fichero app.js
		InLineIntent(){
	        this.$speech.addText('Do you want to order something?'); //Pregunta
	        this.$reprompt.addText('Please answer with yes or no.'); //Repetir pregunta
	        // followUpState sirve enrutar a un estado despues de haber hecho una pregunta (ask)
	        this.followUpState('OrderState')	
	            .ask(this.$speech, this.$reprompt);
	    },
	    OrderState: {
	        YesIntent() { //En caso que yes
	           this.tell('1.KFC\n2.BBQ');
	            //this.toIntent('NextIntent'); //Para salta a siguente estado
	            /*
	            //o enrutar a siguiente estado
	            this.$speech.addText('1.KFC\n2.BBQ');
		        this.$reprompt.addText('Please answer with 1 or 2.');
		        this.followUpState('NextState')	
	            	.ask(this.$speech, this.$reprompt);
	            ****/
	        },
	        NoIntent() { //En caso que no
	           this.tell('BYE');
	        },
	        Unhandled() { //En caso que no sean respuesta correcta (yes o no), entonces volve a pedir respuesta
	            this.$speech.addText('You need to answer with yes, to order something.');
	            this.$reprompt.addText('Please answer with yes or no.');
	            this.ask(this.$speech, this.$reprompt);
	        }
	    },
	    END(){ //END sirve para 'romper' char inLine 
	        this.tell('BYEBYE');
	    }

	    Ex1:
	    Ouput: Do you want to order something?
	    Input: yes
	    Ouput: 1.KFC 2.BBQ

	    Ex2:
	    Input: Do you want to order something?
	    Input: my name is Pepe
	    Ouput: You need to answer with yes, to order something.
	    Input: no
	    Ouput: BYE

	    Ex3:
	    Input: Do you want to order something?
	    Input: STOP
	    Ouput: BYEBYE

- Para las operaciones asíncronas (API) debe de: (https://www.jovo.tech/tutorials/api-call#top-of-page)

	Instalar request promise
		npm install request-promise-native --save
		npm install request --save

	Y agregar **async** a sus funciones de controlador
		const requestPromise = require('request-promise-native');
		app.setHandler({
		    LAUNCH() {
		        return this.toIntent('QuoteIntent');
		    },
		    async QuoteIntent() {
		        const quote = await getRandomQuote();
		        this.tell(quote);
		    },
		});


- Validation de entrada (https://www.jovo.tech/docs/routing/input#validation)

	fichero .json (en-US.json)
		{
			"name": "MyNameIsIntent", //Nombre de intencion
			"phrases": [	//frases para activar intencion
				"{name}",	// {name} es el variable que se define cliente por entrada (inputs)
				"my name is {name}",
				"i am {name}",
				"you can call me {name}"
			],
			"inputs": [
				{
					"name": "",	// variable name
					"type": {	//tipo de variable
						"alexa": "AMAZON.US_FIRST_NAME",
						"dialogflow": "@sys.given-name"
					}
				}
			]
		}

	fichero app.js
		const { Validator, Jovo, ValidationError, IsRequiredValidator, ValidValuesValidator, InvalidValuesValidator } = require('jovo-core'); 
		MyNameIsIntent() {
			const schema = {
	            name: [ //variable de entrada
	                new InvalidValuesValidator([ //Lisa de contenido invalido
	                    'Pepe',
	                    'Maria'
	                ])
	                //new ValidValuesValidator([]) //Lista de contenido valido
	            ]
	        };
	        const validation = this.validate(schema);
	        if(validation.failed('name')) {
	            return this.ask('Name is invalid');
	        }else{
	            this.tell('Hey ${this.$inputs.name.value}!');
	        }
		},



##[DialogFlow Cli](https://github.com/googleapis/nodejs-dialogflow)

[Configurar autenticación y descargar clave de google](https://dialogflow.com/docs/reference/v2-auth-setup)

Cargamos(importar) clave a fichero index.js 
const sessionClient = new dialogflow.SessionsClient({keyFilename: "./{nombre_de_fichero}.json"});

##[Fulfillment en webhook](https://cloud.google.com/dialogflow/docs/tutorials/build-an-agent/create-fulfillment-using-webhook)

##Referencias
[ISO Language Code Table](http://www.lingoes.net/en/translator/langcode.htm)
