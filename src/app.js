'use strict';

// ------------------------------------------------------------------
// APP INITIALIZATION
// ------------------------------------------------------------------

const { App } = require('jovo-framework');
const { Alexa } = require('jovo-platform-alexa');
const { GoogleAssistant } = require('jovo-platform-googleassistant');
const { JovoDebugger } = require('jovo-plugin-debugger');
const { FileDb } = require('jovo-db-filedb');
const { Validator, Jovo, ValidationError, IsRequiredValidator, ValidValuesValidator, InvalidValuesValidator } = require('jovo-core');
const { Dialogflow, FacebookMessenger, Slack } = require('jovo-platform-dialogflow');
const app = new App();
app.use(
    new Dialogflow().use(
        new Slack(),
        new FacebookMessenger()
    ),
    new Alexa(),
    new GoogleAssistant(),
    new JovoDebugger(),
    new FileDb()
);
const requestPromise = require('request-promise-native');

const KEY_CLIENT = "63970cf5f38b48d29bfd71668709243c"; //Client access token, estan ubicado en configuracion de AGENTS -> General ->API KEYS
var lang = "es";
var sessionId = "12345";
var timezone = "Europe/Barcelona";
// ------------------------------------------------------------------
// APP LOGIC
// ------------------------------------------------------------------

app.setHandler({
    
    LAUNCH() {
        //console.log('LAUNCH');
        //return this.toIntent('HelloWorldIntent');

        
        //this.toIntent('InLineIntent');
        
        //this.toIntent('InfoContratIntent');
        //return this.toIntent('QuoteIntent');
    },
    async QuoteIntent() {
        const quote = await getRandomQuote();
        this.tell(quote);
    },
    InLineIntent(){
        this.$speech.addText('Do you want to order something?');
        this.$reprompt.addText('Please answer with yes or no.');
        this.followUpState('OrderState')
            .ask(this.$speech, this.$reprompt);
    },

    HelloWorldIntent() {
        this.ask('Hello World! What\'s your name?', 'Please tell me your name.');

    },

    MyNameIsIntent() {
        //this.tell('Hey ' + this.$inputs.name.value + ', nice to meet you!');
        const schema = {
            name: [
                //new IsRequiredValidator(),      // check if current input is required and present
                new ValidValuesValidator([      // fails if current input value does not match one of the registered values
                    'Lin'
                ])
            ]
        };
        
        const validation = this.validate(schema);
        
        if(validation.failed('name')) {
            return this.ask('Name is invalid (Lin)');
            //return this.toIntent('HelloWorldIntent');
        }else{
            this.tell('Hey ${this.$inputs.name.value}!');
        }
        
        /*if(validation.failed('name')) {
            return this.ask('Please tell me your name again.');
        }*/
        /*
        this.tell(`Hey ${this.$inputs.name.value}!`);
        */
    },

    ToDayIsIntent() {
        var date = new Date();
        this.tell('ToDay is '+date);
        //this.tell(JSON.stringify(this.$request));
    },
    OrderState: {

        YesIntent() {
           this.tell('1.KFC\n2.BBQ');
        },

        NoIntent() {
           this.tell('BYE');
        },
        Unhandled() {
            this.$speech.addText('You need to answer with yes, to order something.');
            this.$reprompt.addText('Please answer with yes or no.');

            this.ask(this.$speech, this.$reprompt);
        }
    },
    async InfoContratIntent() {
        const result = await getInfoContrat(this.$inputs.query.value);
        this.tell(result);
        //this.tell(this.$inputs.query.value);
    },
    END(){
        this.tell('BYEBYE');
    }
});
async function getInfoContrat(query){
    const options = {
        method: 'POST',
        uri: 'https://api.dialogflow.com/v1/query?v=20170712',
        headers: {
            'Authorization': 'Bearer '+KEY_CLIENT,
            'Content-Type': 'application/json'
        },
        body: {
            "lang": lang,
            "query": query,
            "sessionId": sessionId,
            "timezone": timezone
        },
        json: true
    };

    const data = await requestPromise(options);
    const result = data.result.fulfillment.speech;
    console.log("--------------------------------------------");
    console.log(data);
    console.log("--------------------------------------------");
    return result;
}
async function getRandomQuote() {
    const options = {
        method: 'POST',
        uri: 'https://api.dialogflow.com/v1/query?v=20170712',
        headers: {
            'Authorization': 'Bearer '+KEY_CLIENT,
            'Content-Type': 'application/json'
        },
        body: {
            "contexts": [
                "shop"
            ],
            "lang": lang,
            "query": "Definicion de Constitución Española",
            "sessionId": sessionId,
            "timezone": timezone
        },
        json: true
    };
    const data = await requestPromise(options);
    const quote = data.result.fulfillment.speech;
    console.log("--------------------------------------------");
    console.log(data);
    console.log("--------------------------------------------");
    return quote;
}
module.exports.app = app; 